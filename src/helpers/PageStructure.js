import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Dimensions 
} from 'react-native';
import { StackNavigator, TabNavigator, DrawerNavigator, NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import { SideMenu } from '../components/Menus';
import { MainHeader } from '../components/Headers';
import * as colors from './ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const SIDE_MENU_WIDTH = (WINDOW_WIDTH / 4) * 3;

import {
    AboutUsPage,
    ContactPage,
    GaleryPage,
    ImageShowPage,
    HomePage,
    LocationsPage,
    LocationsDetailPage,
    ServiceDatePage,
    ServicesPage,
    ServicesDetailPage,
    TestDrivePage,
} from '../pages';

export const MainNavigator = StackNavigator({
    HomePage: { screen: HomePage },
    AboutUsPage: { screen: AboutUsPage },
    ContactPage: { screen: ContactPage },
    GaleryPage: { screen: GaleryPage },
    ImageShowPage: { screen: ImageShowPage },
    LocationsPage: { screen: LocationsPage },
    LocationsDetailPage: { screen: LocationsDetailPage },
    ServiceDatePage: { screen: ServiceDatePage },
    ServicesPage: { screen: ServicesPage },
    ServicesDetailPage: { screen: ServicesDetailPage },
    TestDrivePage: { screen: TestDrivePage },
  },
  {
    headerMode: 'none',
    lazyLoad: true,
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

export const MainDrawerNavigator = DrawerNavigator(
  {
    home: {screen: MainNavigator},
  },
  {
    contentComponent: SideMenu,
    drawerWidth: SIDE_MENU_WIDTH,
    navigationOptions: ({navigation}) => ({
      drawerLockMode: 'locked-closed'
    })
  }
)