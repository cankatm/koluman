import koluman_balgat from '../../assets/images/koluman_balgat.jpg';
// import otomobilImage from '../../assets/images/otomobil.png';
// import hafifTicariImage from '../../assets/images/hafif_ticari.png';
// import kamyonImage from '../../assets/images/kamyon.png';
// import otobusImage from '../../assets/images/otobus.png';
// import treyImage from '../../assets/images/Treyler.png';
// import unimogImage from '../../assets/images/unimog.png';

import otomobilImage from '../../assets/images/home/otomobil.png';
import hafifTicariImage from '../../assets/images/home/hafif_ticari.png';
import kamyonImage from '../../assets/images/home/kamyon.png';
import otobusImage from '../../assets/images/home/otobus.png';
import treyImage from '../../assets/images/home/treyler.png';
import unimogImage from '../../assets/images/home/unimog.png';

export const locations = [
    {
        id: '1',
        content: `ANKARA BALGAT`,
        info: {
            image: koluman_balgat,
            adress: `Nesimi Mahallesi Gaziantep Caddesi\nNo:70 Şehitkamil / Gaziantep`,
            phone: `0342 437 85 00`,
            fax: `0342 437 84 00`,
            openHours: `Hafta içi: 08:30 - 17:30\n\nCumartesi: 08:30 - 12:30`,
            writing:`Bacon ipsum dolor amet venison bresaola tenderloin short ribs, ball tip burgdoggen rump filet mignon. Short loin jerky kielbasa turkey cow spare ribs pork t-bone pork loin meatball alcatra. Brisket landjaeger pancetta, tri-tip beef ribs frankfurter flank ribeye bacon pastrami pork loin tenderloin meatball. Beef swine chuck kielbasa.`,
            map: ``
        }
    },
    {
        id: '2',
        content: `ANKARA GÖLBAŞI`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
    {
        id: '3',
        content: `İSTANBUL SAMANDIRA`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
    {
        id: '4',
        content: `İSTANBUL GÖZTEPE`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
    {
        id: '5',
        content: `İSTANBUL BÜYÜKDERE`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
    {
        id: '6',
        content: `MERSİN TARSUS`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
    {
        id: '7',
        content: `MERSİN HUZURKENT`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
];

export const services = [
    {
        id: '8',
        content: `ANKARA BALGAT`,
        header: `SERVİSLER-BALGAT`,
        info: {
            image: koluman_balgat,
            adress: `Nesimi Mahallesi Gaziantep Caddesi\nNo:70 Şehitkamil / Gaziantep`,
            phone: `0342 437 85 00`,
            fax: `0342 437 84 00`,
            openHours: `Hafta içi: 08:30 - 17:30\n\nCumartesi: 08:30 - 12:30`,
            writing:`Bacon ipsum dolor amet venison bresaola tenderloin short ribs, ball tip burgdoggen rump filet mignon. Short loin jerky kielbasa turkey cow spare ribs pork t-bone pork loin meatball alcatra. Brisket landjaeger pancetta, tri-tip beef ribs frankfurter flank ribeye bacon pastrami pork loin tenderloin meatball. Beef swine chuck kielbasa.`,
            map: ``
        }
    },
    {
        id: '9',
        content: `ANKARA GÖLBAŞI`,
        header: `SERVİSLER-GÖLBAŞI`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
    {
        id: '10',
        content: `İSTANBUL SAMANDIRA`,
        header: `SERVİSLER-SAMANDIRA`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
    {
        id: '11',
        content: `İSTANBUL GÖZTEPE`,
        header: `SERVİSLER-GÖZTEPE`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
    {
        id: '12',
        content: `İSTANBUL BÜYÜKDERE`,
        header: `SERVİSLER-BÜYÜKDERE`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
    {
        id: '13',
        content: `MERSİN TARSUS`,
        header: `SERVİSLER-TARSUS`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
    {
        id: '14',
        content: `MERSİN HUZURKENT`,
        header: `SERVİSLER-HUZURKENT`,
        info: {
            image: koluman_balgat,
            adress: ``,
            phone: ``,
            fax: ``,
            openHours: ``,
            writing:``,
            map: ``
        }
    },
];

export const homePageButtons = [
    {
        id: '15',
        imageSource: otomobilImage,
        whereTo: ``
    },
    {
        id: '16',
        imageSource: hafifTicariImage,
        whereTo: ``
    },
    {
        id: '17',
        imageSource: kamyonImage,
        whereTo: ``
    },
    {
        id: '18',
        imageSource: otobusImage,
        whereTo: ``
    },
    {
        id: '19',
        imageSource: treyImage,
        whereTo: ``
    },
    {
        id: '20',
        imageSource: unimogImage,
        whereTo: ``
    },
];

export const aboutUsContent = `1965 yılında mümessillik ve ithalat alanında çalışmak üzere kurulmuş bir şirket olan KOLUMAN, bugün, motorlu araçlar, inşaat ve mühendislik, pazarlama ve dış ticaret alanlarını kapsayan geniş bir alanda faaliyet göstermektedir. KOLUMAN, 1984 yılında Otomarsan'a ortak olarak, Mercedes-Benz'in Türkiye'de yeniden yapılanması sırasında, ülkemizin otomotiv pazarında önemli bir sorumluluk almıştır.\n\nFirmamız, Mercedes-Benz Türk A.Ş.’nin Türkiye anabayilerinden biri olup, Mercedes marka araçların satış ve satış sonrası servis hizmetlerini vermesinin yanı sıra, araç üst yapısı ve montaj işleri de yapmaktadır.\n\nKOLUMAN Motorlu Araçlar A.Ş. finans, idare, satış, montaj, üretim ve uluslararası ticaret birimiyle idari yapısını oluştururken, işletme noktalarındaki başarılı hizmet ve servis anlayışı ile özel sektörün yanı sıra, başta T.C. Karayolları, Köy Hizmetleri Gen. Müd., Türk Silahlı Kuvvetleri, Orman Gen. Müd., Devlet Demiryolları olmak üzere bir çok kamu kuruluşuna ve belediyelere yaptığı satışlar ve kurduğu tesislerle devlet sektörüne güvenle hizmetlerini sürdürmektedir.\n\nKOLUMAN Motorlu Araçlar A.Ş.’nin tüm şubeleri ISO 9001:2008 Kalite Yönetim Sistemi, ISO 14001:2004 Çevre Yönetim Sistemi; TS 18001:2007 İş Sağlığı ve Güvenliği Yönetim Sistemi kapsamında TÜV akreditasyonlu belgeye sahip olup; bu sistemleri tüm iş süreçlerinde başarıyla uygulamaya devam etmektedir.\n\nAvrupa standatlarındaki pazarlama ve servis kalitesini imalat alanına da taşıyarak, damper ve tanker üretiminin yanı sıra yangın söndürme üst yapıları, tuzserici, vakumlu süpürge, ön süpürge ve kar bıçağı ekipmanlarını da güvenle piyasaya sürmeye başlamıştır.\n\nYıllardır Türkiye pazarında taşımacılık sektörünün sorunları ile yakından ilgilenen KOLUMAN, Koluman Otomotiv Endüstri A.Ş. bünyesinde Tarsus’taki 80.000 m2’lik fabrikasında üretilen treylerlerin satışını da uygun finans koşulları ile yapmaktadır. Bunun yanında, pazarladığı ürünler arasında, MERCEDES-BENZ UNIMOG'un ayrıcalıklı bir yeri vardır.\n\nŞirketimiz 2017 yılı sonu itibariyle Türkiye genelinde 5 Şube, 11 Showroom, 776 personeli, 1.740.662.584-TL cirosu, tamamı ödenmiş 193.000.000.-TL sermayesi ile otomotiv sektörünün önemli firmalarından biri haline gelen şirketimiz, Fortune dergisinin her yıl yayınladığı Türkiye'nin En Büyük 500 Özel Şirketi listesinde 2017 cirosu ile 111. sırada yer almıştır.\n\nŞirketin en büyük hissedarı, aynı zamanda Mercedes-Benz Türk A.Ş.’nin de ortağı olan Koluman Holding A.Ş.’dir.\n\nŞirketimizin yönetim merkezi Ankara'dadır.\n\nDiğer Grup Şirketleri\nKoluman Otomotiv Endüstri A.Ş.\nHAMA Oto Kiralama ve Çevre Hizmetleri A.Ş.\nKoluman Sigorta Aracılık Hizmetleri A.Ş.\nKoluman İnşaat ve Araç Muayene İstasyon İşletmeciliği A.Ş.\nMonde Motorlu Araçlar Tic.ve San. A.Ş.`;