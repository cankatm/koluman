import { Platform } from 'react-native';

export const statusBarHeight = Platform.OS === 'ios' ? 22 : 0;
export const bottomMenuHeight = 60;
export const bottomMenuInnerButtonHeight = 50;
export const headerHeight = 40;
export const headerSubtitleHeight = 30;
export const bottomMenuBottomDistance = 16;
export const blackButtonHeight = 80;
export const triangleHeight = 40;
export const detailButtonHeight = 0;
export const locationButtonHeight = 120;