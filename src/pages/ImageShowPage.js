import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Image,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';
import ImageZoom from 'react-native-image-pan-zoom';
import Icon from 'react-native-vector-icons/FontAwesome';

import { ZoomView } from '../components/ZoomView';
import styles from './styles';
import imageSource from '../../assets/images/koluman_balgat.jpg';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

class ImageShowPage extends Component {

    render() {
        const imgsrc = this.props.navigation.getParam('imageUrl');
        return (
            <View style={{ flex: 1, backgroundColor: 'black' }} >
                <View style={{ position: 'absolute', right: 16, top: 16, zIndex: 60 }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} >
                        <View style={{ width: 80, height: 80, zIndex: 60 }} >
                            <Icon size={80} name={'times'} color={'#FFF'} />
                        </View>
                    </TouchableOpacity>
                </View>

                <ImageZoom 
                    cropWidth={WINDOW_WIDTH}
                    cropHeight={WINDOW_HEIGHT}
                    imageWidth={WINDOW_WIDTH}
                    imageHeight={WINDOW_HEIGHT}
                    minScale={1}
                    maxScale={4}
                >
                    <Image style={{width:WINDOW_WIDTH, height:WINDOW_HEIGHT, resizeMode: 'contain' }}
                        source={{ uri: imgsrc }}/>
                </ImageZoom>
            </View>
        );
    }
}

export default withNavigation(ImageShowPage);