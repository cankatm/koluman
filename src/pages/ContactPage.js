import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TextInput,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    Modal,
    Platform,
    Picker,
    Alert
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import Triangle from 'react-native-triangle';

import { SubtitleHeader, MainHeader } from '../components/Headers';
import { BottomMenuButton, HomePageButton } from '../components/Buttons';
import { BottomMenu } from '../components/Menus';
import { TriangleArea } from '../components/Areas';
import { switchBottomMenu } from '../actions';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';
import styles from './styles';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const ICON_SIZE = 32;
const marginBottomValue = 8;
      

class ContactPage extends Component {
    constructor(props){
        super(props)
        this.state = {
            name: '',
            email: '',
            phone: '',
            city: '',
            adress: '',
            message: '',
            modalCityVisible: false,
        }
    }

    render() {
        const { 
            name, 
            email, 
            phone, 
            city, 
            adress, 
            message,
            modalCityVisible, 
        } = this.state;
        
        const renderBottomMenu = () => {
            if (this.props.bottomReducer.bottomMenuOpened) {
                return (
                    <View style={styles.homePageBottomMenuContainerStyle} >
                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight * 2 + marginBottomValue * 2} 
                            title='Servisler' 
                            to='ServicesPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight + marginBottomValue} 
                            title='Lokasyonlar' 
                            to='LocationsPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={0}
                            title='Test Sürüşü' 
                            to='TestDrivePage'
                        />

                        <TriangleArea />
                    </View>
                )
            }
        }

        const renderCityDropdownButton = () => {
            if (Platform.OS === 'ios') {
                return (
                    <TouchableOpacity onPress={() => this.setState({ modalCityVisible: true })} >
                        <View style={styles.serviceDatePageDropdownContainerStyle} >
                            <Text style={{ marginLeft: 8, fontSize: 14 }} >{city}</Text>

                            <View style={{ marginRight: 8 }} >
                                <Icon size={ICON_SIZE} name='md-arrow-dropdown' color='black' />
                            </View>
                        </View>
                    </TouchableOpacity>
                )
            }

            return(
                <View style={styles.serviceDatePageAndroidDropdownContainerStyle}>
                    <Picker
                        selectedValue={city}
                        style={styles.serviceDatePageAndroidPickerStyle}
                        onValueChange={(itemValue, itemIndex) => this.setState({city: itemValue})}
                    >
                        <Picker.Item label="" value="" />
                        <Picker.Item label="Ankara" value="ankara" />
                        <Picker.Item label="İstanbul" value="istanbul" />
                        <Picker.Item label="İzmir" value="izmir" />
                        <Picker.Item label="Antalya" value="antalya" />
                        <Picker.Item label="Eskişehir" value="eskisehir" />
                    </Picker>
                    
                    <View style={styles.serviceDatePageAndroidDropdownIconContainerStyle} >
                        <View style={{ marginRight: 8 }} >
                            <Icon size={ICON_SIZE} name='md-arrow-dropdown' color='black' />
                        </View>
                    </View>
                </View>
            )
        }

        const handleSubmitButton = () => {
            if (name !== '' && 
                email !== '' && 
                phone !== '' && 
                city !== '' && 
                adress !== '' && 
                message !== '') {
                    // return yollama işlemi burada
            }
            else {
                return (
                    Alert.alert(
                        'Eksik Alan',
                        'Lütfen eksik alanları doldurunuz.',
                        [
                            {text: 'Tamam', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                    )
                )
            }
        }

        return (
            <View style={{ flex: 1, backgroundColor: colors.lightestGrey }} >
                <MainHeader />
                <SubtitleHeader subtitleHeader='İLETİŞİM FORMU' />
                
                <KeyboardAwareScrollView>
                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8, marginTop: 32 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Adınız & Soyadınız</Text>

                        <TextInput
                            {...this.props}
                            style={styles.serviceDatePageTextInputStyle}
                            maxLength = {30}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            placeholderTextColor={colors.darkestGrey}
                            selectionColor={colors.lightBlue}
                            onChangeText={value => this.setState({ name: value })}
                            value={name}
                        />
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >E-Posta Adresiniz</Text>

                        <TextInput
                            {...this.props}
                            style={styles.serviceDatePageTextInputStyle}
                            maxLength = {30}
                            keyboardType='email-address'
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            placeholderTextColor={colors.darkestGrey}
                            selectionColor={colors.lightBlue}
                            onChangeText={value => this.setState({ email: value })}
                            value={email}
                        />
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Telefon Numaranız</Text>

                        <TextInput
                            {...this.props}
                            style={styles.serviceDatePageTextInputStyle}
                            maxLength = {10}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            placeholderTextColor={colors.darkestGrey}
                            selectionColor={colors.lightBlue}
                            onChangeText={value => this.setState({ phone: value })}
                            value={phone}
                        />
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Yaşadığınız Şehir</Text>

                        {renderCityDropdownButton()}
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Adresiniz</Text>

                        <TextInput
                            {...this.props}
                            style={[styles.serviceDatePageTextInputStyle, { height: 80 }]}
                            maxLength = {300}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            placeholderTextColor={colors.darkestGrey}
                            selectionColor={colors.lightBlue}
                            onChangeText={value => this.setState({ adress: value })}
                            value={adress}
                            multiline
                            blurOnSubmit
                        />
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Mesajınız</Text>

                        <TextInput
                            {...this.props}
                            style={[styles.serviceDatePageTextInputStyle, { height: 80 }]}
                            maxLength = {300}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            placeholderTextColor={colors.darkestGrey}
                            selectionColor={colors.lightBlue}
                            onChangeText={value => this.setState({ message: value })}
                            value={message}
                            multiline
                            blurOnSubmit
                        />
                    </View>

                    <View style={styles.serviceDatePageDatePickButtonContainerStyle} >
                        <TouchableOpacity onPress={() => handleSubmitButton()} >
                            <View style={styles.serviceDatePageDatePickButtonInnerContainerStyle} >
                                <Text style={styles.serviceDatePageDatePickButtonTextStyle} >Gönder</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{ height: 32 }} />
                        
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalCityVisible}
                        onRequestClose={() => {
                            console.log('Modal has been closed.')
                        }}
                    >
                        <View style={styles.serviceDatePageModalContainerStyle}>
                            <View style={styles.serviceDatePageModalButtonsContainerStyle} >
                                <TouchableOpacity onPress={() => this.setState({ modalCityVisible: false, modalBrandVisible: false })} >
                                    <Text style={styles.serviceDatePageModalButtonTextStyle}>Onayla</Text>
                                </TouchableOpacity>
                            </View>

                            <Picker
                                selectedValue={city}
                                style={styles.serviceDatePageModalPickerStyle}
                                onValueChange={(itemValue, itemIndex) => this.setState({city: itemValue})}
                            >
                                    <Picker.Item label="" value="" />
                                    <Picker.Item label="Ankara" value="ankara" />
                                    <Picker.Item label="İstanbul" value="istanbul" />
                                    <Picker.Item label="İzmir" value="izmir" />
                                    <Picker.Item label="Antalya" value="antalya" />
                                    <Picker.Item label="Eskişehir" value="eskisehir" />
                            </Picker>
                        </View>

                        <TouchableOpacity activeOpacity={1} onPress={() => this.setState({ modalCityVisible: false, modalBrandVisible: false })} >
                            <View style={styles.serviceDatePageModalEmptyAreaStyle} />
                        </TouchableOpacity>
                    </Modal>

                    <View style={{ height: defaults.triangleHeight }} />

                </KeyboardAwareScrollView>

                {renderBottomMenu()}

                <BottomMenu />

                <TriangleArea />

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default connect(mapStateToProps, { switchBottomMenu })(ContactPage);