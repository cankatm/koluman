import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Image,
    Dimensions,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import Triangle from 'react-native-triangle';

import { SubtitleHeader, MainHeader } from '../components/Headers';
import { BottomMenuButton, HomePageButton } from '../components/Buttons';
import { BottomMenu } from '../components/Menus';
import { TriangleArea } from '../components/Areas';
import { aboutUsContent } from '../helpers/EmbeddedDatas';
import * as defaults from '../helpers/DefaultValues';
import { switchBottomMenu } from '../actions';
import styles from './styles';
import headerImage from '../../assets/images/koluman_balgat.jpg';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const marginBottomValue = 8;

class AboutUsPage extends Component {
    render() {
        const renderBottomMenu = () => {
            if (this.props.bottomReducer.bottomMenuOpened) {
                return (
                    <View style={styles.homePageBottomMenuContainerStyle} >
                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight * 2 + marginBottomValue * 2} 
                            title='Servisler' 
                            to='ServicesPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight + marginBottomValue} 
                            title='Lokasyonlar' 
                            to='LocationsPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={0}
                            title='Test Sürüşü' 
                            to='TestDrivePage'
                        />

                        <TriangleArea />
                    </View>
                )
            }
        }
        
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }} >
                <MainHeader />
                <SubtitleHeader subtitleHeader='HAKKIMIZDA' />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <Image source={headerImage} style={{ width: WINDOW_WIDTH, height: 100 }} />

                    <View style={{ width: WINDOW_WIDTH }} >
                        <View style={{ width: WINDOW_WIDTH - 64, marginLeft: 32, marginTop: 16 }}>
                            <Text style={{ textAlign: 'justify' }} >{aboutUsContent}</Text>
                        </View>
                    </View>
                    <View style={{ height: 96 }} />
                </ScrollView>

                {renderBottomMenu()}

                <BottomMenu />

                <TriangleArea />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default connect(mapStateToProps, { switchBottomMenu })(AboutUsPage);