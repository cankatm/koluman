import AboutUsPage from './AboutUsPage';
import ContactPage from './ContactPage';
import GaleryPage from './GaleryPage';
import ImageShowPage from './ImageShowPage';
import HomePage from './HomePage';
import LocationsPage from './LocationsPage';
import LocationsDetailPage from './LocationsDetailPage';
import ServiceDatePage from './ServiceDatePage';
import ServicesPage from './ServicesPage';
import ServicesDetailPage from './ServicesDetailPage';
import TestDrivePage from './TestDrivePage';

export {
    AboutUsPage,
    ContactPage,
    GaleryPage,
    ImageShowPage,
    HomePage,
    LocationsPage,
    LocationsDetailPage,
    ServiceDatePage,
    ServicesPage,
    ServicesDetailPage,
    TestDrivePage,
};