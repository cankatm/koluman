import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Image,
    ScrollView,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import Triangle from 'react-native-triangle';

import { SubtitleHeader, MainHeader } from '../components/Headers';
import { BottomMenuButton, HomePageButton } from '../components/Buttons';
import { BottomMenu } from '../components/Menus';
import { InfoArea, TriangleArea } from '../components/Areas';
import { switchBottomMenu } from '../actions';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';
import styles from './styles';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const marginBottomValue = 8;

class ServicesDetailPage extends Component {
    render() {
        const { content, info, header } = this.props.navigation.state.params;

        const renderBottomMenu = () => {
            if (this.props.bottomReducer.bottomMenuOpened) {
                return (
                    <View style={styles.homePageBottomMenuContainerStyle} >
                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight * 2 + marginBottomValue * 2} 
                            title='Servisler' 
                            to='ServicesPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight + marginBottomValue} 
                            title='Lokasyonlar' 
                            to='LocationsPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={0}
                            title='Test Sürüşü' 
                            to='TestDrivePage'
                        />

                        <TriangleArea />
                    </View>
                )
            }
        }
        
        return (
            <View style={{flex: 1, backgroundColor: colors.white}}>
                <MainHeader />
                <SubtitleHeader subtitleHeader={header} showBackButton={true} />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ backgroundColor: colors.white }}
                >
                    <Image source={info.image} style={styles.locationsDetailPageImageStyle} />
                    <InfoArea iconName='home' text={info.adress} />
                    <InfoArea iconName='phone' text={info.phone} />
                    <InfoArea iconName='fax' text={info.fax} />
                    <InfoArea iconName='home' text={info.openHours} />

                    <View>
                        <View style={styles.locationsDetailPageTextContainerStyle} >
                            <Text style={styles.locationsDetailPageTextStyle} >{info.writing}</Text>
                        </View>
                    </View>

                </ScrollView>

                <View style={styles.locationsDetailPageButtonAreaContainerStyle} >
                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('ServiceDatePage')}} >
                        <View style={[styles.locationsDetailPageButtonContainerStyle, { borderRightWidth: 1 }]} >
                            <View style={{ marginTop: 12 }} >
                                <Icon size={42} name='ios-mail' color={colors.black} />
                            </View>
                            <Text style={styles.locationsDetailPageButtonTextStyle} >SERVİS RANDEVUSU</Text>
                            <View style={{ height: defaults.detailButtonHeight }} />
                        </View>
                    </TouchableOpacity>
                    
                    <TouchableOpacity>
                        <View style={[styles.locationsDetailPageButtonContainerStyle, { borderLeftWidth: 1 }]} >
                            <View style={{ marginTop: 16 }} >
                                <Icon size={42} name='ios-navigate' color={colors.black} />
                            </View>
                            <Text style={styles.locationsDetailPageButtonTextStyle} >HARİTA</Text>
                            <View style={{ height: defaults.detailButtonHeight }} />
                        </View>
                    </TouchableOpacity>
                </View>

                {renderBottomMenu()}

                <BottomMenu />

                <TriangleArea />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default connect(mapStateToProps, { switchBottomMenu })(ServicesDetailPage);