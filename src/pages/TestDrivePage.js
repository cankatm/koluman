import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import Triangle from 'react-native-triangle';

import { SubtitleHeader, MainHeader } from '../components/Headers';
import { BottomMenuButton, HomePageButton } from '../components/Buttons';
import { BottomMenu } from '../components/Menus';
import { TriangleArea } from '../components/Areas';
import { switchBottomMenu } from '../actions';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';
import styles from './styles';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const marginBottomValue = 8;

class TestDrivePage extends Component {
    render() {
        const renderBottomMenu = () => {
            if (this.props.bottomReducer.bottomMenuOpened) {
                return (
                    <View style={styles.homePageBottomMenuContainerStyle} >
                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight * 2 + marginBottomValue * 2} 
                            title='Servisler' 
                            to='ServicesPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight + marginBottomValue} 
                            title='Lokasyonlar' 
                            to='LocationsPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={0}
                            title='Test Sürüşü' 
                            to='TestDrivePage'
                        />

                        <TriangleArea />
                    </View>
                )
            }
        }
        
        return (
            <View style={{ flex: 1 }} >
                <MainHeader />
                <SubtitleHeader subtitleHeader='TEST SÜRÜŞÜ' />

                {renderBottomMenu()}

                <BottomMenu />

                <TriangleArea />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default connect(mapStateToProps, { switchBottomMenu })(TestDrivePage);