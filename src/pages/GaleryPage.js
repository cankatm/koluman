import React, { Component } from 'react';
import { 
    View, 
    Text, 
    FlatList,
    Dimensions,
    ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import Triangle from 'react-native-triangle';

import { SubtitleHeader, MainHeader } from '../components/Headers';
import { BottomMenuButton, HomePageButton, ImageButton } from '../components/Buttons';
import { BottomMenu } from '../components/Menus';
import { TriangleArea } from '../components/Areas';
import { switchBottomMenu } from '../actions';

import * as defaults from '../helpers/DefaultValues';
import styles from './styles';

import * as API from '../helpers/Api'

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const marginBottomValue = 8;

class GaleryPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photos: [{
                imageUrl: ''
            }],
            render: false
        };
    }

    componentWillMount () {
        this.fetchPhotos()
    }

    fetchPhotos() {
        fetch(API.URL_GALERI)
        .then((resp) => resp.json()) // Transform the data into json
        .then((data) => {
            if(data.success)
                this.setState({ photos: data.result, render: true })
        })   
    }

    renderPhotos(){
        if(this.state.render){
            return(
                <FlatList
                    data={this.state.photos}
                    renderItem={({item}) => <ImageButton source={item.imageUrl} />}
                    keyExtractor={(item) => item.id}
                    style={{flexDirection: 'column'}}
                    numColumns={3}
                />
            )
        } else {
            return(<ActivityIndicator style={styles.loadingIndicator} size="large" color="#fff" />)
        }
    }

    render() {
        const renderBottomMenu = () => {
            if (this.props.bottomReducer.bottomMenuOpened) {
                return (
                    <View style={styles.homePageBottomMenuContainerStyle} >
                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight * 2 + marginBottomValue * 2} 
                            title='Servisler' 
                            to='ServicesPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight + marginBottomValue} 
                            title='Lokasyonlar' 
                            to='LocationsPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={0}
                            title='Test Sürüşü' 
                            to='TestDrivePage'
                        />

                        <TriangleArea />
                    </View>
                )
            }
        }
        
        return (
            <View style={{ flex: 1, backgroundColor: 'black' }} >
                <MainHeader />
                <SubtitleHeader subtitleHeader='GALERİ' />
                {this.renderPhotos()}
                

                <View style={{ height: 64 }} />

                {renderBottomMenu()}

                <BottomMenu />

                <TriangleArea />

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default connect(mapStateToProps, { switchBottomMenu })(GaleryPage);