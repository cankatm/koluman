import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TextInput,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    Modal,
    Platform,
    Picker,
    Alert
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Triangle from 'react-native-triangle';

import { SubtitleHeader, MainHeader } from '../components/Headers';
import { BottomMenuButton, HomePageButton } from '../components/Buttons';
import { BottomMenu } from '../components/Menus';
import { TriangleArea } from '../components/Areas';
import { switchBottomMenu } from '../actions';
import * as defaults from '../helpers/DefaultValues';
import * as colors from '../helpers/ColorPalette';
import styles from './styles';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const ICON_SIZE = 32;

const marginBottomValue = 8;
      

class ServiceDatePage extends Component {
    constructor(props){
        super(props)
        this.state = {
            name: '',
            email: '',
            phone: '',
            plate: '',
            date: "15-05-2016",
            time: "00:00",
            modalCityVisible: false,
            modalBrandVisible: false,
            city: '',
            brand: ''
        }
    }

    render() {
        const { 
            name, 
            email, 
            phone, 
            plate, 
            date, 
            time, 
            modalCityVisible, 
            modalBrandVisible, 
            city, 
            brand 
        } = this.state;
        
        const renderBottomMenu = () => {
            if (this.props.bottomReducer.bottomMenuOpened) {
                return (
                    <View style={styles.homePageBottomMenuContainerStyle} >
                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight * 2 + marginBottomValue * 2} 
                            title='Servisler' 
                            to='ServicesPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight + marginBottomValue} 
                            title='Lokasyonlar' 
                            to='LocationsPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={0}
                            title='Test Sürüşü' 
                            to='TestDrivePage'
                        />

                        <TriangleArea />
                    </View>
                )
            }
        }

        const renderCityDropdownButton = () => {
            if (Platform.OS === 'ios') {
                return (
                    <TouchableOpacity onPress={() => this.setState({ modalCityVisible: true })} >
                        <View style={styles.serviceDatePageDropdownContainerStyle} >
                            <Text style={{ marginLeft: 8, fontSize: 14 }} >{city}</Text>

                            <View style={{ marginRight: 8 }} >
                                <Icon size={ICON_SIZE} name='md-arrow-dropdown' color='black' />
                            </View>
                        </View>
                    </TouchableOpacity>
                )
            }

            return(
                <View style={styles.serviceDatePageAndroidDropdownContainerStyle}>
                    <Picker
                        selectedValue={city}
                        style={styles.serviceDatePageAndroidPickerStyle}
                        onValueChange={(itemValue, itemIndex) => this.setState({city: itemValue})}
                    >
                        <Picker.Item label="" value="" />
                        <Picker.Item label="Ankara" value="ankara" />
                        <Picker.Item label="İstanbul" value="istanbul" />
                        <Picker.Item label="İzmir" value="izmir" />
                        <Picker.Item label="Antalya" value="antalya" />
                        <Picker.Item label="Eskişehir" value="eskisehir" />
                    </Picker>
                    
                    <View style={styles.serviceDatePageAndroidDropdownIconContainerStyle} >
                        <View style={{ marginRight: 8 }} >
                            <Icon size={ICON_SIZE} name='md-arrow-dropdown' color='black' />
                        </View>
                    </View>
                </View>
            )
        }

        const renderBrandDropdownButton = () => {
            if (Platform.OS === 'ios') {
                return (
                    <TouchableOpacity onPress={() => this.setState({ modalBrandVisible: true })} >
                        <View style={styles.serviceDatePageDropdownContainerStyle} >
                            <Text style={{ marginLeft: 8, fontSize: 14 }} >{brand}</Text>

                            <View style={{ marginRight: 8 }} >
                                <Icon size={ICON_SIZE} name='md-arrow-dropdown' color='black' />
                            </View>
                        </View>
                    </TouchableOpacity>
                )
            }

            return(
                <View style={styles.serviceDatePageAndroidDropdownContainerStyle}>
                    <Picker
                        selectedValue={brand}
                        style={styles.serviceDatePageAndroidPickerStyle}
                        onValueChange={(itemValue, itemIndex) => this.setState({brand: itemValue})}
                    >
                        <Picker.Item label="" value="" />
                        <Picker.Item label="Mercedes 3 Serisi" value="3 serisi" />
                        <Picker.Item label="Mercedes 5 Serisi" value="5 serisi" />
                    </Picker>
                    
                    <View style={styles.serviceDatePageAndroidDropdownIconContainerStyle} >
                        <View style={{ marginRight: 8 }} >
                            <Icon size={ICON_SIZE} name='md-arrow-dropdown' color='black' />
                        </View>
                    </View>
                </View>
            )
        }

        const handleSubmitButton = () => {
            if (name !== '' && 
                email !== '' && 
                phone !== '' && 
                plate !== '' && 
                city !== '' && 
                brand !== '') {
                    // return yollama işlemi burada
            }
            else {
                return (
                    Alert.alert(
                        'Eksik Alan',
                        'Lütfen eksik alanları doldurunuz.',
                        [
                            {text: 'Tamam', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                    )
                )
            }
        }

        return (
            <View style={{ flex: 1, backgroundColor: colors.lightestGrey }} >
                <MainHeader />
                <SubtitleHeader subtitleHeader='SERVİS FORMU' />
                
                <KeyboardAwareScrollView>
                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8, marginTop: 32 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Adınız & Soyadınız</Text>

                        <TextInput
                            {...this.props}
                            style={styles.serviceDatePageTextInputStyle}
                            maxLength = {30}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            placeholderTextColor={colors.darkestGrey}
                            selectionColor={colors.lightBlue}
                            onChangeText={value => this.setState({ name: value })}
                            value={name}
                        />
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >E-Posta Adresiniz</Text>

                        <TextInput
                            {...this.props}
                            style={styles.serviceDatePageTextInputStyle}
                            maxLength = {30}
                            keyboardType='email-address'
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            placeholderTextColor={colors.darkestGrey}
                            selectionColor={colors.lightBlue}
                            onChangeText={value => this.setState({ email: value })}
                            value={email}
                        />
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Telefon Numaranız</Text>

                        <TextInput
                            {...this.props}
                            style={styles.serviceDatePageTextInputStyle}
                            maxLength = {10}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            placeholderTextColor={colors.darkestGrey}
                            selectionColor={colors.lightBlue}
                            onChangeText={value => this.setState({ phone: value })}
                            value={phone}
                        />
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Yaşadığınız Şehir</Text>

                        {renderCityDropdownButton()}
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Plaka</Text>

                        <TextInput
                            {...this.props}
                            style={styles.serviceDatePageTextInputStyle}
                            maxLength = {30}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            placeholderTextColor={colors.darkestGrey}
                            selectionColor={colors.lightBlue}
                            onChangeText={value => this.setState({ plate: value })}
                            value={plate}
                        />
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Marka/Model/Seri</Text>

                        {renderBrandDropdownButton()}
                    </View>

                    <View style={[styles.serviceDatePageInputContainerStyle, { marginVertical: 8 }]}>
                        <Text style={styles.serviceDatePageInputTextStyle} >Randevu Tarih/Saat</Text>

                        <View style={styles.serviceDatePageDateContainerStyle} >

                            <View style={styles.serviceDatePageDateInnerContainerStyle} >
                                <View style={styles.serviceDatePageDateIconContainerStyle} >
                                    <Icon size={ICON_SIZE} name='md-calendar' color='black' />
                                </View>
                                <View style={{ paddingRight: 8 }}>
                                    <DatePicker
                                        style={styles.serviceDatePageDatePickerStyle}
                                        date={date}
                                        mode="date"
                                        placeholder="Tarih Seçin"
                                        format="DD-MM-YYYY"
                                        minDate="01-05-2016"
                                        maxDate="01-06-2050"
                                        confirmBtnText="Onayla"
                                        androidMode='spinner'
                                        cancelBtnText="İptal"
                                        iconComponent={<Icon size={32} name='md-arrow-dropdown' color='black' />}
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 0,
                                                    borderWidth: 0
                                                }
                                        }}
                                        onDateChange={(date) => {this.setState({date: date})}}
                                    />
                                </View>
                            </View>


                            <View style={styles.serviceDatePageClockInnerContainerStyle} >
                                <View style={styles.serviceDatePageClockIconContainerStyle} >
                                    <Icon size={ICON_SIZE} name='md-time' color='black' />
                                </View>
                                <View style={{ paddingRight: 8 }}>
                                    <DatePicker
                                        style={styles.serviceDatePageClockPickerStyle}
                                        date={time}
                                        mode="time"
                                        placeholder="Saat Seçin"
                                        format="HH.mm"
                                        minDate="08.00"
                                        maxDate="24.00"
                                        confirmBtnText="Onayla"
                                        cancelBtnText="İptal"
                                        iconComponent={<Icon size={32} name='md-arrow-dropdown' color='black' />}
                                        androidMode='spinner'
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 0,
                                                    borderWidth: 0
                                                }
                                        }}
                                        onDateChange={(time) => {this.setState({time: time})}}
                                    />
                                </View>
                            </View>

                        </View>
                    </View>

                    <View style={styles.serviceDatePageDatePickButtonContainerStyle} >
                        <TouchableOpacity onPress={() => handleSubmitButton()} >
                            <View style={styles.serviceDatePageDatePickButtonInnerContainerStyle} >
                                <Text style={styles.serviceDatePageDatePickButtonTextStyle} >Randevu Al</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{ height: 32 }} />
                        
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalCityVisible}
                        onRequestClose={() => {
                            console.log('Modal has been closed.')
                        }}
                    >
                        <View style={styles.serviceDatePageModalContainerStyle}>
                            <View style={styles.serviceDatePageModalButtonsContainerStyle} >
                                <TouchableOpacity onPress={() => this.setState({ modalCityVisible: false, modalBrandVisible: false })} >
                                    <Text style={styles.serviceDatePageModalButtonTextStyle}>Onayla</Text>
                                </TouchableOpacity>
                            </View>

                            <Picker
                                selectedValue={city}
                                style={styles.serviceDatePageModalPickerStyle}
                                onValueChange={(itemValue, itemIndex) => this.setState({city: itemValue})}
                            >
                                    <Picker.Item label="" value="" />
                                    <Picker.Item label="Ankara" value="ankara" />
                                    <Picker.Item label="İstanbul" value="istanbul" />
                                    <Picker.Item label="İzmir" value="izmir" />
                                    <Picker.Item label="Antalya" value="antalya" />
                                    <Picker.Item label="Eskişehir" value="eskisehir" />
                            </Picker>
                        </View>

                        <TouchableOpacity activeOpacity={1} onPress={() => this.setState({ modalCityVisible: false, modalBrandVisible: false })} >
                            <View style={styles.serviceDatePageModalEmptyAreaStyle} />
                        </TouchableOpacity>
                    </Modal>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalBrandVisible}
                        onRequestClose={() => {
                            console.log('Modal has been closed.')
                        }}
                    >
                        <View style={styles.serviceDatePageModalContainerStyle}>
                            <View style={styles.serviceDatePageModalButtonsContainerStyle} >
                                <TouchableOpacity onPress={() => this.setState({ modalCityVisible: false, modalBrandVisible: false })} >
                                    <Text style={styles.serviceDatePageModalButtonTextStyle}>Onayla</Text>
                                </TouchableOpacity>
                            </View>

                            <Picker
                                selectedValue={brand}
                                style={styles.serviceDatePageModalPickerStyle}
                                onValueChange={(itemValue, itemIndex) => this.setState({brand: itemValue})}
                            >
                                    <Picker.Item label="" value="" />
                                    <Picker.Item label="Mercedes 3 Serisi" value="3 serisi" />
                                    <Picker.Item label="Mercedes 5 Serisi" value="5 serisi" />
                            </Picker>
                        </View>

                        <TouchableOpacity activeOpacity={1} onPress={() => this.setState({ modalCityVisible: false, modalBrandVisible: false })} >
                            <View style={styles.serviceDatePageModalEmptyAreaStyle} />
                        </TouchableOpacity>
                    </Modal>
                    
                    <View style={{ height: defaults.triangleHeight }} />

                </KeyboardAwareScrollView>

                {renderBottomMenu()}

                <BottomMenu />

                <TriangleArea />

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default connect(mapStateToProps, { switchBottomMenu })(ServiceDatePage);