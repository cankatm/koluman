import React, { Component } from 'react';
import { 
    View, 
    Text, 
    ScrollView, 
    TouchableOpacity,
    Dimensions,
    Image,
    FlatList
} from 'react-native';
import { connect } from 'react-redux';
import Triangle from 'react-native-triangle';

import { MainHeader } from '../components/Headers';
import { BottomMenuButton, HomePageButton } from '../components/Buttons';
import { BottomMenu } from '../components/Menus';
import { HomeSlider } from '../components/Sliders';
import { TriangleArea } from '../components/Areas';
import { switchBottomMenu } from '../actions';

import styles from './styles';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';
import koluman2BottomImage from '../../assets/images/koluman2BottomImage.png';
import { homePageButtons } from '../helpers/EmbeddedDatas';

import * as API from '../helpers/Api'

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const marginBottomValue = 8;

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sliders: [],
            render: false
        };
    }

    componentWillMount () {
        this.fetchSliders()
    }

    fetchSliders() {
        fetch(API.URL_SLIDER)
        .then((resp) => resp.json())
        .then((data) => {
            if(data.success)
                this.setState({ sliders: data.result, render: true })
        })   

        // fetch(API.URL_SUBELER)
        // .then((resp) => resp.json())
        // .then((data) => {
        //     console.log('subeler')
        //     console.log(data);            
        // })
        
       
    }

    render() {
        const renderBottomMenu = () => {
            if (this.props.bottomReducer.bottomMenuOpened) {
                return (
                    <View style={styles.homePageBottomMenuContainerStyle} >
                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight * 2 + marginBottomValue * 2} 
                            title='Servisler' 
                            to='ServicesPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight + marginBottomValue} 
                            title='Lokasyonlar' 
                            to='LocationsPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={0}
                            title='Test Sürüşü' 
                            to='TestDrivePage'
                        />

                        <TriangleArea />
                    </View>
                )
            }
        }
        
        return (
            <View style={{ flex: 1, backgroundColor: colors.metallicGrey }} >
                <MainHeader />
                <ScrollView                 
                    overScrollMode={'never'}
                    bounces={false}
                    >
                    <HomeSlider sliders={this.state.sliders}/>                    

                    <FlatList
                        data={homePageButtons}
                        renderItem={({item}) => <HomePageButton whereTo={item.whereTo} imageSource={item.imageSource} />}
                        keyExtractor={(item) => item.id}
                        style={{flexDirection: 'column'}}
                        numColumns={3}
                    />

                    <View style={styles.homePageImageContainerStyle} >
                        <View style={{ height: 32 }} />
                        <Image source={koluman2BottomImage} style={styles.homePageImageStyle} />
                    </View>

                    <View style={{ height: 80 }} />

                </ScrollView>

                {renderBottomMenu()}

                <BottomMenu />

                <TriangleArea />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default connect(mapStateToProps, { switchBottomMenu })(HomePage);