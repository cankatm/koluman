import React, { Component } from 'react';
import { 
    View, 
    Text, 
    FlatList, 
    Dimensions,
    ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import Triangle from 'react-native-triangle';

import { SubtitleHeader, MainHeader } from '../components/Headers';
import { BottomMenuButton, HomePageButton, BlackButton } from '../components/Buttons';
import { BottomMenu } from '../components/Menus';
import { TriangleArea } from '../components/Areas';
import { locations } from '../helpers/EmbeddedDatas';
import { switchBottomMenu } from '../actions';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';
import styles from './styles';

import * as API from '../helpers/Api'

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const marginBottomValue = 8;

class LocationsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            locations: [],
            render: false
        };
    }

    componentWillMount () {
        this.fetchLocations()
    }

    fetchLocations(){
        fetch(API.URL_SUBELER)
        .then((resp) => resp.json())
        .then((data) => {
            console.log(data)
            if(data.success)
                this.setState({ locations: data.result, render: true })        
        })   
    }

    renderLocations(){
        if(this.state.render){
            return(
                <FlatList
                    data={this.state.locations}
                    renderItem={({item}) => <BlackButton content={item} whereTo='LocationsDetailPage' />}
                    keyExtractor={(item) => item.id.toString()}
                />
            )
        } else {
            return(<ActivityIndicator style={styles.loadingIndicator} size="large" color="#fff" />)
        }
    }

    render() {
        const renderBottomMenu = () => {
            if (this.props.bottomReducer.bottomMenuOpened) {
                return (
                    <View style={styles.homePageBottomMenuContainerStyle} >
                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight * 2 + marginBottomValue * 2} 
                            title='Servisler' 
                            to='ServicesPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={defaults.bottomMenuInnerButtonHeight + marginBottomValue} 
                            title='Lokasyonlar' 
                            to='LocationsPage'
                        />

                        <BottomMenuButton 
                            marginBottomProp={0}
                            title='Test Sürüşü' 
                            to='TestDrivePage'
                        />

                        <TriangleArea />
                    </View>
                )
            }
        }
        
        return (
            <View style={{ flex: 1, backgroundColor: colors.black }} >
                <MainHeader />
                <SubtitleHeader subtitleHeader='LOKASYONLARIMIZ' />
                {this.renderLocations()}
                <View style={{ height: 64 }} />

                {renderBottomMenu()}

                <BottomMenu />

                <TriangleArea />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default connect(mapStateToProps, { switchBottomMenu })(LocationsPage);