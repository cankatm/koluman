import React from 'react';
import { 
    StyleSheet, 
    Dimensions,
    Platform 
} from 'react-native';

import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const BOTTOM_MENU_HEIGHT = defaults.bottomMenuHeight;
const IMAGE_ADD_SIZE = 30;
const LOCATION_BOTTOM_BUTTON_HEIGHT = defaults.locationButtonHeight;
const EXTRA_WIDTH = 64;
const CLOCK_PICKER_WIDTH = 60;
const PICKER_ANDROID_HEIGHT = 40;

export default StyleSheet.create({
    homePageBottomMenuButtonOuterContainerStyle: {
        position: 'absolute',
        bottom: defaults.bottomMenuBottomDistance,
        left: WINDOW_WIDTH / 2 - BOTTOM_MENU_HEIGHT / 2,
        zIndex: 5
    },
    homePageBottomMenuButtonContainerStyle: {
        width: BOTTOM_MENU_HEIGHT,
        height: BOTTOM_MENU_HEIGHT,
        borderRadius: BOTTOM_MENU_HEIGHT / 2,
        backgroundColor: colors.black,
        alignItems: 'center',
        justifyContent: 'center'
    },
    homePageBottomMenuButtonImageStyle: {
        width: BOTTOM_MENU_HEIGHT + IMAGE_ADD_SIZE,
        height: BOTTOM_MENU_HEIGHT + IMAGE_ADD_SIZE,
        borderRadius: BOTTOM_MENU_HEIGHT / 2 - IMAGE_ADD_SIZE / 2,
        resizeMode: 'contain'
    },
    homePageBottomMenuContainerStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT - defaults.headerHeight - defaults.statusBarHeight,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        zIndex: 3,
        flex: 1,
        position: 'absolute',
        bottom: 0,
        left: 0
    },
    homePageBestsContainerStyle: { 
        width: WINDOW_WIDTH, 
        height: 60, 
        backgroundColor: colors.lightBlue, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    homePageImageContainerStyle: { 
        width: WINDOW_WIDTH, 
        height: 160, 
        backgroundColor: colors.metallicGrey 
    },
    homePageImageStyle: { 
        width: WINDOW_WIDTH, 
        height: 80, 
        resizeMode: 'contain' 
    },
    homePageBestsTextStyle: { 
        fontSize: 20, 
        color: colors.white 
    },
    locationsDetailPageImageStyle: { 
        width: WINDOW_WIDTH,
        height: 80,
        resizeMode: 'cover'
    },
    locationsDetailPageTextContainerStyle: { 
        width: WINDOW_WIDTH - EXTRA_WIDTH, 
        marginLeft: EXTRA_WIDTH / 2, 
        marginVertical: 32, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    locationsDetailPageTextStyle: { 
        fontSize: 14, 
        textAlign: 'center' 
    },
    locationsDetailPageButtonAreaContainerStyle: { 
        width: WINDOW_WIDTH, 
        height: LOCATION_BOTTOM_BUTTON_HEIGHT, 
        flexDirection: 'row'
    },
    locationsDetailPageButtonContainerStyle: { 
        width: WINDOW_WIDTH / 2, 
        height: LOCATION_BOTTOM_BUTTON_HEIGHT, 
        alignItems: 'center', 
        justifyContent: 'flex-start',
        borderTopWidth: 2,
        borderColor: colors.lightGrey
    },
    locationsDetailPageButtonTextStyle: { 
        fontSize: 12, 
        color: colors.black,
        marginBottom: 2
    },
    serviceDatePageInputContainerStyle: {
        width: WINDOW_WIDTH,
        marginVertical: 32,
    },
    serviceDatePageInputTextStyle: { 
        marginLeft: EXTRA_WIDTH / 2, 
        marginBottom: 4, 
        fontSize: 16 
    },
    serviceDatePageTextInputStyle: { 
        fontSize: 14,
        width: WINDOW_WIDTH - EXTRA_WIDTH, 
        marginLeft: EXTRA_WIDTH / 2,
        paddingHorizontal: 8, 
        paddingVertical: 8, 
        opacity: 1, 
        backgroundColor: colors.white, 
        color: colors.darkestGrey,
    },
    serviceDatePageDropdownContainerStyle: { 
        width: WINDOW_WIDTH - EXTRA_WIDTH, 
        height: PICKER_ANDROID_HEIGHT, 
        backgroundColor: colors.white, 
        marginLeft: EXTRA_WIDTH / 2, 
        alignItems: 'center', 
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    serviceDatePageAndroidDropdownContainerStyle: { 
        width: WINDOW_WIDTH - EXTRA_WIDTH,
        height: PICKER_ANDROID_HEIGHT, 
        justifyContent: 'space-between', 
        flexDirection: 'row', 
        marginLeft: EXTRA_WIDTH / 2 
    },
    serviceDatePageAndroidPickerStyle: { 
        height: PICKER_ANDROID_HEIGHT, 
        width: WINDOW_WIDTH -EXTRA_WIDTH, 
        backgroundColor: 'transparent', 
        zIndex: 100 
    },
    serviceDatePageAndroidDropdownIconContainerStyle: { 
        width: WINDOW_WIDTH -EXTRA_WIDTH, 
        height: PICKER_ANDROID_HEIGHT, 
        backgroundColor: colors.white, 
        alignItems: 'flex-end', 
        justifyContent: 'center', 
        position: 'absolute', 
        left: 0, 
        top: 0 
    },
    serviceDatePageDateContainerStyle: { 
        flexDirection: 'row', 
        width: WINDOW_WIDTH - EXTRA_WIDTH, 
        marginLeft: EXTRA_WIDTH / 2 
    },
    serviceDatePageDateInnerContainerStyle: { 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center', 
        backgroundColor: colors.white, 
        marginRight: 8 
    },
    serviceDatePageDateIconContainerStyle: { 
        marginRight: 8, 
        paddingLeft: 8 
    },
    serviceDatePageDatePickerStyle: {
        width: WINDOW_WIDTH - EXTRA_WIDTH - CLOCK_PICKER_WIDTH - 108
    },
    serviceDatePageClockInnerContainerStyle: { 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center', 
        backgroundColor: colors.white 
    },
    serviceDatePageClockIconContainerStyle: { 
        marginRight: 8, 
        paddingLeft: 8 
    },
    serviceDatePageClockPickerStyle: {
        width: CLOCK_PICKER_WIDTH
    },
    serviceDatePageDatePickButtonContainerStyle: { 
        width: WINDOW_WIDTH - EXTRA_WIDTH, 
        marginLeft: EXTRA_WIDTH / 2, 
        alignItems: 'center', 
        justifyContent: 'flex-end', 
        flexDirection: 'row', 
        marginTop: 16 
    },
    serviceDatePageDatePickButtonInnerContainerStyle: { 
        padding: 16,  
        backgroundColor: colors.lightBlue, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    serviceDatePageDatePickButtonTextStyle: { 
        color: colors.white, 
        fontSize: 18 
    },
    serviceDatePageModalContainerStyle: {
        position: 'absolute', 
        bottom: 0, 
        left: 0, 
        width: WINDOW_WIDTH, 
        height: WINDOW_HEIGHT / 3, 
        alignItems: 'center',
        backgroundColor: colors.white,
        zIndex: 3
    },
    serviceDatePageModalButtonsContainerStyle: { 
        width: WINDOW_WIDTH - EXTRA_WIDTH, 
        flexDirection: 'row', 
        justifyContent: 'flex-end', 
        alignItems: 'center', 
        marginTop: 16
    },
    serviceDatePageModalButtonTextStyle: { 
        fontSize: 14,
        color: 'green'
    },
    serviceDatePageModalPickerStyle: { 
        height: PICKER_ANDROID_HEIGHT, 
        width: WINDOW_WIDTH -EXTRA_WIDTH, 
        zIndex: 100 
    },
    serviceDatePageModalEmptyAreaStyle: { 
        width: WINDOW_WIDTH, 
        height: WINDOW_HEIGHT, 
        backgroundColor: 'transparent' 
    },
    loadingIndicator: {
        marginTop: 10
    }
});