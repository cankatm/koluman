import { SWITCH_BOTTOM_MENU } from '../actions';

const INITIAL_STATE = {
    bottomMenuOpened: false
};
  
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SWITCH_BOTTOM_MENU:
            return { ...state, bottomMenuOpened: action.payload };
        default:
            return state;
    }
};
  