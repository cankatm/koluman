export const SWITCH_BOTTOM_MENU = 'SWITCH_BOTTOM_MENU';

export const switchBottomMenu = (value) => {
    return {
        type: SWITCH_BOTTOM_MENU,
        payload: value
    };
}
