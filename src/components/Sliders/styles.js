import React from 'react';
import { 
    StyleSheet, 
    Dimensions,
    Platform 
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const SLIDER_HEIGHT = 140;
const BUTTON_HEIGHT = 60;

const HEADER_HEIGHT = defaults.headerHeight;

export default StyleSheet.create({
    homeSliderScrollViewStyle: { 
        width: WINDOW_WIDTH,
        backgroundColor: colors.black
    },
    homeSliderImageContainerStyle: { 
        width: WINDOW_WIDTH, 
        height: SLIDER_HEIGHT, 
    },
    homeSliderImageStyle: { 
        width: WINDOW_WIDTH, 
        height: SLIDER_HEIGHT, 
        resizeMode: 'cover' 
    },
    homePageBestsContainerStyle: { 
        width: WINDOW_WIDTH, 
        height: 60, 
        backgroundColor: colors.lightBlue, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    homePageBestsTextStyle: { 
        fontSize: 20, 
        color: colors.white 
    },
    homeSliderShowForwardButtonOuterContainer: { 
        position: 'absolute', 
        right: 16, 
        top: SLIDER_HEIGHT / 2 - BUTTON_HEIGHT / 2
    },
    homeSliderShowForwardButtonInnerContainer: { 
        width: BUTTON_HEIGHT, 
        height: BUTTON_HEIGHT, 
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    homeSliderShowBackwardButtonOuterContainer: { 
        position: 'absolute', 
        left: 16, 
        top: SLIDER_HEIGHT / 2 - BUTTON_HEIGHT / 2
    },
    homeSliderShowBackwardButtonInnerContainer: { 
        width: BUTTON_HEIGHT, 
        height: BUTTON_HEIGHT, 
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
});