import React, { Component } from 'react';
import { 
    View, 
    Text, 
    ScrollView,
    Dimensions,
    TouchableOpacity,
    Image
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import * as colors from '../../helpers/ColorPalette';
import styles from './styles';
import firstImage from '../../../assets/images/1.jpg';
import secondImage from '../../../assets/images/2.jpg';
import thirdImage from '../../../assets/images/3.jpg';
import image1 from '../../../assets/images/home_slider_1.png';
import * as API from '../../helpers/Api'

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

//3 resim koydum diye ShowForward fonksiyonunda 3 üzerinden yaptım, sayı değişirse o da değişecek
class HomeSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            x:0,
            text: 'En iyiler için alternatif yoktur.',
        };
    }

    render() {
        const showForward = () => {
            if (!(this.state.x === 3 - 1)) {
                return (
                    <View style={styles.homeSliderShowForwardButtonOuterContainer} >
                        <TouchableOpacity onPress={() => {
                            this._scrollView.scrollTo({ x: (this.state.x + 1) * WINDOW_WIDTH, y: 0, animated: true }),
                            this.setState({ x: this.state.x + 1, text: this.props.sliders[this.state.x + 1].text })
                        }} >
                            <View style={styles.homeSliderShowForwardButtonInnerContainer} >
                                <Icon size={42} name='ios-arrow-forward' color={colors.white} />
                            </View>
                        </TouchableOpacity>
                    </View>
                )
            }
        }

        const showBackward = () => {
            if (!(this.state.x === 0)) {
                return (
                    <View style={styles.homeSliderShowBackwardButtonOuterContainer} >
                        <TouchableOpacity onPress={() => {
                            this._scrollView.scrollTo({ x: (this.state.x - 1) * WINDOW_WIDTH, y: 0, animated: true }),
                            this.setState({ x: this.state.x - 1, text: this.props.sliders[this.state.x - 1].text  })

                        }} >
                            <View style={styles.homeSliderShowBackwardButtonInnerContainer} >
                                <Icon size={42} name='ios-arrow-back' color={colors.white} />
                            </View>
                        </TouchableOpacity>
                    </View>
                )
            }
        }
        const slides = this.props.sliders.map((data) => {
            return (
                <View key={data.id} style={styles.homeSliderImageContainerStyle}>
                    <Image 
                        source={{ uri: API.URL_FILEBASE+data.imageUrl, cache: 'force-cache' }} style={styles.homeSliderImageStyle} 
                        />
                </View>
            )
          })

        return (
            <View>
                <ScrollView
                    ref={(c) => { this._scrollView = c; }}
                    horizontal
                    pagingEnabled
                    showsHorizontalScrollIndicator={false}
                    scrollEnabled={false}
                    style={styles.homeSliderScrollViewStyle}
                >
                    {slides}

                </ScrollView>
                {showForward()}
                {showBackward()}
                <View style={styles.homePageBestsContainerStyle} >
                        <Text style={styles.homePageBestsTextStyle} >{this.state.text}</Text>
                </View>
            </View>
            
        );
    }
}

export default HomeSlider;