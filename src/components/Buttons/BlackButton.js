import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity 
} from 'react-native';
import { withNavigation } from 'react-navigation';

import styles from './styles';

class BlackButton extends Component {
    render() {
        const { content, header, whereTo } = this.props;

        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate(whereTo, { content, header })} >
                <View style={styles.blackButtonContainerStyle}>
                    <Text style={styles.blackButtonTextStyle} >{content.adi}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export default withNavigation(BlackButton);