import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Image,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';
import styles from './styles';

class ImageButton extends Component {
    render() {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ImageShowPage', { imageUrl: this.props.source })} >
                <View style={styles.imageButtonContainerStyle} >
                    <Image
                        style={{ width: '100%', height: '100%' }}
                        source={{ uri : this.props.source }}
                    />
                    {/* <Icon size={32} name='md-images' color={colors.white} /> */}
                </View>
            </TouchableOpacity>
        );
    }
}

export default withNavigation(ImageButton);