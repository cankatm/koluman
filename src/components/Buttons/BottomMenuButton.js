import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity,
    Image
} from 'react-native';
import { withNavigation, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';

import { switchBottomMenu } from '../../actions';
import styles from './styles';

class BottomMenuButton extends Component {

    _renderIcon(){
        if(this.props.to == 'ServicesPage')
        {
            return(<Image  style={{ resizeMode: 'center' }} 
            source={require('../../../assets/images/bottommenu/servisler.png')} />)
        }
        else if(this.props.to == 'LocationsPage'){
            return(<Image  style={{ resizeMode: 'center' }} 
            source={require('../../../assets/images/bottommenu/lokasyonlar.png')} />)
        } else {
            return(<Image  style={{ resizeMode: 'center' }} 
            source={require('../../../assets/images/bottommenu/test_surusu.png')} />)
        }
    }

    render() {
        const { title, marginBottomProp, to } = this.props;
        const handleButtonPress = () => {
            const resetAction= NavigationActions.reset({
                index:0,
                actions:[
                    NavigationActions.navigate({ routeName: to })
                ],
            })
            this.props.switchBottomMenu(false)
            this.props.navigation.dispatch(resetAction)
        }
        return (
            <View style={styles.bottomMenuInnerContainerStyle} >
                <TouchableOpacity onPress={() => handleButtonPress()} >
                    <View style={[styles.bottomMenuInnerFlexContainerStyle, { marginBottom: marginBottomProp }]} >
                        <View style={styles.bottomMenuIconContainerStyle} >
                            {this._renderIcon()}
                        </View>

                        <View style={styles.bottomMenuTextContainerStyle} >
                            <Text style={styles.bottomMenuTextStyle}>{title}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default withNavigation(connect(mapStateToProps, { switchBottomMenu })(BottomMenuButton));