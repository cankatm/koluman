import React from 'react';
import { 
    StyleSheet, 
    Dimensions,
    Platform 
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const BOTTOM_MENU_HEIGHT = defaults.bottomMenuHeight;
const BOTTOM_MENU_ICON_CONTAINER_HEIGHT = defaults.bottomMenuInnerButtonHeight;

export default StyleSheet.create({
    bottomMenuInnerContainerStyle: { 
        position: 'absolute', 
        bottom:  defaults.bottomMenuHeight + defaults.bottomMenuBottomDistance * 2,
        left: WINDOW_WIDTH / 2 - defaults.bottomMenuHeight / 2,
        zIndex: 50
    },
    bottomMenuInnerFlexContainerStyle: { 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    bottomMenuIconContainerStyle: { 
        width: BOTTOM_MENU_ICON_CONTAINER_HEIGHT, 
        height: BOTTOM_MENU_ICON_CONTAINER_HEIGHT, 
        borderRadius: BOTTOM_MENU_ICON_CONTAINER_HEIGHT / 2, 
        backgroundColor: colors.white, 
        alignItems: 'center', 
        justifyContent: 'center',
        marginLeft: (BOTTOM_MENU_HEIGHT - BOTTOM_MENU_ICON_CONTAINER_HEIGHT) / 2
    },
    bottomMenuTextContainerStyle: { 
        backgroundColor: colors.white, 
        alignItems: 'center', 
        justifyContent: 'center', 
        width: 80, 
        marginLeft: 4 
    },
    bottomMenuTextStyle: { 
        paddingVertical: 2 
    },
    blackButtonContainerStyle: { 
        backgroundColor: colors.black,
        width: WINDOW_WIDTH,
        height: defaults.blackButtonHeight,
        alignItems: 'center',
        justifyContent: 'center'
    },
    blackButtonTextStyle: { 
        fontSize: 16,
        color: colors.white
    },
    homePageButtonContainerStyle: { 
        backgroundColor: colors.black,
        width: (WINDOW_WIDTH / 3),
        height: (WINDOW_WIDTH / 3),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.white
    },
    homePageButtonImageStyle: { 
        width: (WINDOW_WIDTH / 3),
        height: (WINDOW_WIDTH / 3),
        resizeMode: 'contain'
    },
    imageButtonContainerStyle: { 
        backgroundColor: colors.black,
        width: (WINDOW_WIDTH / 3),
        height: (WINDOW_WIDTH / 3),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.white
    },
});