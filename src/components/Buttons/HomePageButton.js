import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Image,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

import styles from './styles';

class HomePageButton extends Component {
    constructor(props) {
        super(props);
        this.state = { pressStatus: false };
      }
    _onHideUnderlay(){
        this.setState({ pressStatus: true });
    }
      _onShowUnderlay(){
        this.setState({ pressStatus: false });
    }
    render() {
        const { whereTo, imageSource } = this.props;
        return (
            <TouchableOpacity 
                activeOpacity={1}
                style={ this.state.pressStatus ? styleItem.buttonPress : styleItem.button }
                onPressIn={this._onHideUnderlay.bind(this)}
                onPressOut={this._onShowUnderlay.bind(this)}>
                <View style={styles.homePageButtonContainerStyle} >
                    <Image source={imageSource} style={styles.homePageButtonImageStyle} />
                </View>
            </TouchableOpacity>
        );
    }
}

const styleItem = StyleSheet.create({
    button: {
      borderColor: '#000066',
      borderWidth: 0,
      borderRadius: 0,
    },
    buttonPress: {
      //width: '30%', 
      borderColor: '#007dae',
      borderWidth: 0,
      borderRadius: 0,
    },
  });

export default HomePageButton;