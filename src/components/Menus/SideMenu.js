import React, {Component} from 'react';
import styles from './styles';
import {NavigationActions, withNavigation} from 'react-navigation';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconIon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import { switchBottomMenu } from '../../actions';
import * as colors from '../../helpers/ColorPalette';

class SideMenu extends Component {
  navigateToScreen = (route) => () => {
      const resetAction= NavigationActions.reset({
        index:0,
        actions:[
              NavigationActions.navigate({ routeName: route })
        ],
      })
      this.props.switchBottomMenu(false)
      this.props.navigation.dispatch(resetAction)
  }

  render () {
    return (
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.headerContainer} />
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('AboutUsPage')}>
              Hakkımızda
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('GaleryPage')}>
                Galeri
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('ServicesPage')}>
                Servisler
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('LocationsPage')}>
                Lokasyonlar
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('ContactPage')}>
                İletişim
              </Text>
            </View>
          </View>



          <View style={{ marginTop: 24 }} >
            <TouchableOpacity>
              <View style={styles.navSocialSectionStyle}>
                <View style={styles.navSocialIconContainerStyle} >
                  <Icon size={16} name='facebook-f' color={colors.lightBlue} />
                </View>
                <Text style={[styles.navSocialItemStyle, { paddingLeft: 16 }]}>Facebook</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{ marginTop: 8 }} >
            <TouchableOpacity>
              <View style={styles.navSocialSectionStyle}>
                <View style={styles.navSocialIconContainerStyle} >
                  <Icon size={16} name='instagram' color={colors.lightBlue} />
                </View>
                <Text style={[styles.navSocialItemStyle, { paddingLeft: 16 }]}>Instagram</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{ marginTop: 8 }} >
            <TouchableOpacity>
              <View style={styles.navSocialSectionStyle}>
                <View style={styles.navSocialIconContainerStyle} >
                  <Icon size={16} name='twitter' color={colors.lightBlue} />
                </View>
                <Text style={[styles.navSocialItemStyle, { paddingLeft: 16 }]}>Twitter</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{ marginTop: 8 }} >
            <TouchableOpacity>
              <View style={styles.navSocialSectionStyle}>
                <View style={styles.navSocialIconContainerStyle} >
                  <IconIon size={16} name='logo-googleplus' color={colors.lightBlue} />
                </View>
                <Text style={[styles.navSocialItemStyle, { paddingLeft: 16 }]}>Google Plus</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{ marginTop: 8 }} >
            <TouchableOpacity>
              <View style={styles.navSocialSectionStyle}>
                <View style={styles.navSocialIconContainerStyle} >
                  <Icon size={16} name='youtube' color={colors.lightBlue} />
                </View>
                <Text style={[styles.navSocialItemStyle, { paddingLeft: 16 }]}>Youtube</Text>
              </View>
            </TouchableOpacity>
          </View>



        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return { 
      bottomReducer: state.bottomReducer,
  };
}

export default connect(mapStateToProps, { switchBottomMenu })(SideMenu);