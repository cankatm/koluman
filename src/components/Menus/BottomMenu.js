import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    Dimensions,
    Image,
    Animated,
    Easing
} from 'react-native';
import { connect } from 'react-redux';

import { BottomMenuButton } from '../Buttons';
import { switchBottomMenu } from '../../actions';
import styles from './styles';
import mercedesLogo from '../../../assets/images/mercedes_logo.png';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

class BottomMenu extends Component {
    constructor (props) {
        super(props)
        this.RotateValueHolder = new Animated.Value(0);
        this.state = {
            open: false
        }
    }

    StartImageRotateFunction () {
        this.setState({ open: !this.state.open })
        if(this.state.open){
            this.RotateValueHolder.setValue(1)        
            Animated.timing(
            this.RotateValueHolder,
            {
                toValue: 0,
                duration: 300,
                easing: Easing.ease
            }
            ).start()    
        }
        else{
            this.RotateValueHolder.setValue(0)        
            Animated.timing(
            this.RotateValueHolder,
            {
                toValue: 1,
                duration: 300,
                easing: Easing.ease
            }
            ).start()    
        }
          
    }
    componentDidMount() {
        
    }

    render() {
        const RotateData = this.RotateValueHolder.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '120deg']
        })
        return (
            <View style={styles.homePageBottomMenuButtonOuterContainerStyle}>
                <TouchableOpacity activeOpacity={0.85} onPress={() => { this.StartImageRotateFunction(); this.props.switchBottomMenu(!this.props.bottomReducer.bottomMenuOpened)}} >
                    <View style={styles.homePageBottomMenuButtonContainerStyle}>
                        <Animated.Image source={mercedesLogo} style={ [styles.homePageBottomMenuButtonImageStyle,{transform: [{rotate: RotateData}]}] } />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default connect(mapStateToProps, { switchBottomMenu })(BottomMenu);