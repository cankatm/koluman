import React from 'react';
import { 
    StyleSheet, 
    Dimensions,
    Platform 
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const HEADER_HEIGHT = defaults.headerHeight;

const BOTTOM_MENU_HEIGHT = defaults.bottomMenuHeight;
const IMAGE_ADD_SIZE = 18;

export default StyleSheet.create({
    mainHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        height: HEADER_HEIGHT,
        backgroundColor: colors.red,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: Platform.OS === 'ios' ? defaults.statusBarHeight : 0,
    },
    headerContainer: {
      height: 48,
      backgroundColor: colors.darkestGrey
    },
    container: {
      paddingTop: 20,
      flex: 1,
      backgroundColor: colors.darkestGrey
    },
    navItemStyle: {
      paddingVertical: 16,
      paddingLeft: 32,
      color: colors.white
    },
    navSectionStyle: {
      backgroundColor: colors.darkestGrey,
      borderBottomWidth: 2,
      borderColor: colors.black
    },
    navSocialSectionStyle: {
      backgroundColor: colors.darkestGrey,
      flexDirection: 'row',
    },
    navSocialItemStyle: {
      paddingVertical: 16,
      paddingLeft: 32,
      color: colors.lightBlue
    },
    navSocialIconContainerStyle: { 
      marginLeft: 32, 
      alignItems: 'center', 
      justifyContent: 'center' 
    },
    homePageBottomMenuButtonOuterContainerStyle: {
        position: 'absolute',
        bottom: defaults.bottomMenuBottomDistance,
        left: WINDOW_WIDTH / 2 - BOTTOM_MENU_HEIGHT / 2,
        zIndex: 5
    },
    homePageBottomMenuButtonContainerStyle: {
        width: BOTTOM_MENU_HEIGHT,
        height: BOTTOM_MENU_HEIGHT,
        borderRadius: BOTTOM_MENU_HEIGHT / 2,
        backgroundColor: colors.black,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 12
    },
    homePageBottomMenuButtonImageStyle: {
        width: BOTTOM_MENU_HEIGHT + IMAGE_ADD_SIZE,
        height: BOTTOM_MENU_HEIGHT + IMAGE_ADD_SIZE,
        borderRadius: BOTTOM_MENU_HEIGHT / 2 - IMAGE_ADD_SIZE / 2,
        resizeMode: 'contain',        
    },
    homePageBottomMenuContainerStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT - defaults.headerHeight - defaults.statusBarHeight,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        zIndex: 3,
        flex: 1,
        position: 'absolute',
        bottom: 0,
        left: 0
    },
    homePageBestsContainerStyle: { 
        width: WINDOW_WIDTH, 
        height: 60, 
        backgroundColor: colors.lightBlue, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    homePageBestsTextStyle: { 
        fontSize: 20, 
        color: colors.white 
    },
});