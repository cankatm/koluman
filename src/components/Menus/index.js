import BottomMenu from './BottomMenu';
import SideMenu from './SideMenu';
import styles from './styles';

export { 
    BottomMenu,
    SideMenu,
    styles 
};