import React from 'react';
import { 
    StyleSheet, 
    Dimensions,
    Platform 
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const HEADER_HEIGHT = defaults.headerHeight;

export default StyleSheet.create({
    mainHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        height: HEADER_HEIGHT,
        backgroundColor: colors.black,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainHeaderIOSStatusBarAreaStyle: {
        width: WINDOW_WIDTH,
        height: defaults.statusBarHeight,
        backgroundColor: colors.black,
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainHeaderLogoStyle: { 
        width: 100, 
        height: defaults.headerHeight, 
        resizeMode: 'contain' 
    },
    subtitleHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        height: defaults.headerSubtitleHeight,
        backgroundColor: colors.lightBlue,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    subtitleHeaderIconOuterContainerStyle: { 
        position: 'absolute', 
        right: 0, 
        top: 0, 
    },
    subtitleHeaderIconContainerStyle: { 
        width: 60, 
        height: defaults.headerSubtitleHeight,
        backgroundColor: colors.black,
        alignItems: 'center',
        justifyContent: 'center'
    },
    subtitleHeaderTextStyle: {
        color: colors.white
    },
});