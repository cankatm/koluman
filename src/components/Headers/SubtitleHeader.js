import React, { Component } from 'react';
import { 
    View, 
    Text,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import * as colors from '../../helpers/ColorPalette';
import styles from './styles';

class SubtitleHeader extends Component {
    render() {
        const { subtitleHeader, showBackButton } = this.props;
        const renderTitle = () => {
            if (showBackButton) {
                return (
                    <View style={styles.subtitleHeaderIconOuterContainerStyle} >
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} >
                            <View style={styles.subtitleHeaderIconContainerStyle} >
                                <Icon size={24} name='ios-arrow-back' color={colors.white} />
                            </View>
                        </TouchableOpacity>
                    </View>
                )
            }
        }
        return (
            <View style={styles.subtitleHeaderContainerStyle}>
                <Text style={styles.subtitleHeaderTextStyle}>{subtitleHeader}</Text>
                {renderTitle()}
            </View>
        );
    }
}

export default withNavigation(SubtitleHeader);