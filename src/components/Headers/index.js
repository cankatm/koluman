import MainHeader from './MainHeader';
import SubtitleHeader from './SubtitleHeader';
import BackHeader from './BackHeader';
import styles from './styles';

export { 
    MainHeader,
    SubtitleHeader,
    BackHeader,
    styles 
};