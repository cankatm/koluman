import React, { Component } from 'react';
import { 
    View, 
    Text,  
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { withNavigation, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';

import { switchBottomMenu } from '../../actions';
import styles from './styles';
import * as colors from '../../helpers/ColorPalette';

class BackHeader extends Component {
    handleImageButtonPress = () => {
        const resetAction= NavigationActions.reset({
          index:0,
          actions:[
                NavigationActions.navigate({ routeName: 'HomePage' })
          ],
        })
        this.props.switchBottomMenu(false)
        this.props.navigation.dispatch(resetAction)
    }

    render() {
        const handleButtonPress = () => {
            this.props.switchBottomMenu(false),
            this.props.navigation.goBack()
        }

        return (
            <View>
                <View style={styles.mainHeaderIOSStatusBarAreaStyle} />
                <View style={styles.mainHeaderContainerStyle} >
                    <View style={{ position: 'absolute', left: 16, top: 4, }}>
                        <TouchableOpacity onPress={() => handleButtonPress()}>
                            <View style={{alignItems: 'center', flexDirection: 'row', width: 40, height: 40 }} >
                                <Icon size={32} name='ios-arrow-back' color={colors.white} />
                            </View>
                        </TouchableOpacity>
                    </View>


                    <TouchableOpacity onPress={() => this.handleImageButtonPress()} >
                        <Text style={{ color: colors.white }} >KOLUMAN</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default withNavigation(connect(mapStateToProps, { switchBottomMenu })(BackHeader));