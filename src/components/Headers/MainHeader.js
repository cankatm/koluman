import React, { Component } from 'react';
import { 
    View, 
    Text,  
    TouchableOpacity,
    Image
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { withNavigation, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';

import { switchBottomMenu } from '../../actions';
import styles from './styles';
import * as colors from '../../helpers/ColorPalette';

import kolumanLogo from '../../../assets/images/koluman_logo.png';

class MainHeader extends Component {
    handleImageButtonPress = () => {
        const resetAction= NavigationActions.reset({
          index:0,
          actions:[
                NavigationActions.navigate({ routeName: 'HomePage' })
          ],
        })
        this.props.switchBottomMenu(false)
        this.props.navigation.dispatch(resetAction)
    }

    render() {
        return (
            <View>
                <View style={styles.mainHeaderIOSStatusBarAreaStyle} />
                <View style={styles.mainHeaderContainerStyle} >
                    <View style={{ position: 'absolute', left: 16, top: 4, }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DrawerOpen')}>
                            <View style={{alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }} >
                                <Icon size={32} name='md-menu' color={colors.white} />
                                <Text style={{ marginLeft: 4, marginBottom: 2, color: colors.white }} >MENU</Text>
                            </View>
                        </TouchableOpacity>
                    </View>


                    <TouchableOpacity onPress={() => this.handleImageButtonPress()} >
                        <Image source={kolumanLogo} style={styles.mainHeaderLogoStyle} />
                    </TouchableOpacity>

                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        bottomReducer: state.bottomReducer,
    };
}

export default withNavigation(connect(mapStateToProps, { switchBottomMenu })(MainHeader));