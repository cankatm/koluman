import InfoArea from './InfoArea';
import TriangleArea from './TriangleArea';
import styles from './styles';

export { 
    InfoArea,
    TriangleArea,
    styles 
};