import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import * as colors from '../../helpers/ColorPalette';
import styles from './styles';

//clock fax home

class InfoArea extends Component {
    render() {
        const { text, iconName } = this.props;
        return (
            <View style={styles.infoAreaContainerStyle} >
                <View style={styles.infoAreaIconContainerStyle} >
                    <Icon size={24} name={iconName} color={colors.black} />
                </View>
                <View style={styles.infoAreaTextContainerStyle} >
                    <Text style={styles.infoAreaTextStyle}>{text}</Text>
                </View>
            </View>
        );
    }
}

export default InfoArea;