import React, { Component } from 'react';
import { 
    View, 
    Text, 
    Dimensions
} from 'react-native';
import Triangle from 'react-native-triangle';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';
import styles from './styles';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

class TriangleArea extends Component {
    render() {
        return (
            <View style={{ flex: 1 }} >
                <View style={{ position: 'absolute', bottom: 0, left: - 4, width: WINDOW_WIDTH, height: defaults.triangleHeight + 2 }} >
                    <Triangle
                        width={WINDOW_WIDTH + 8}
                        height={defaults.triangleHeight + 2}
                        color={colors.black}
                        direction={'up'}
                    />
                </View>

                <View style={{ position: 'absolute', bottom: 0, left: 0, width: WINDOW_WIDTH, height: defaults.triangleHeight }} >
                    <Triangle
                        width={WINDOW_WIDTH}
                        height={defaults.triangleHeight}
                        color={colors.lightBlue}
                        direction={'up'}
                    />
                </View>
            </View>
        );
    }
}

export default TriangleArea;