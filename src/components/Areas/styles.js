import React from 'react';
import { 
    StyleSheet, 
    Dimensions,
    Platform 
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const BOTTOM_MENU_HEIGHT = defaults.bottomMenuHeight;
const BOTTOM_MENU_ICON_CONTAINER_HEIGHT = defaults.bottomMenuInnerButtonHeight;

export default StyleSheet.create({
    infoAreaContainerStyle: { 
        width: WINDOW_WIDTH,
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: colors.lightGrey,
    },
    infoAreaIconContainerStyle: { 
        width: 32,
        paddingVertical: 16,
        marginLeft: 32,
        marginRight: 16,
        alignItems: 'center',
        justifyContent: 'center',
    },
    infoAreaTextContainerStyle: { 
        paddingVertical: 16,
        alignItems: 'flex-start',
        width: WINDOW_WIDTH - 82
    },
    infoAreaTextStyle: { 
        color: colors.black,
        fontSize: 14
    },
});