import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  StatusBar,
  Platform
} from 'react-native';
import { Provider } from 'react-redux';

import { MainDrawerNavigator } from './src/helpers/PageStructure';
import store from './src/store';

export default class App extends React.Component {
  render() {
    const renderStatusBar = () => {
      if (Platform.OS === 'ios') {
        return <StatusBar barStyle='light-content' />;
      }
      return <StatusBar hidden />
    }
    return (
      <Provider store={store}>
        <View style={styles.container}>
          {renderStatusBar()}
          <MainDrawerNavigator />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
